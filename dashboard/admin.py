from django.contrib import admin
from .models import Product, Order, Equipment
from django.contrib.auth.models import Group


admin.site.site_header = "Psrs inventory Dashboard"

class ProductAdmin(admin.ModelAdmin):
    list_display = ('name','category', 'quantity')
    list_filter = ('category',)

class EquipmentAdmin(admin.ModelAdmin):
    list_display = ('name','type','make','seno','status','mac','typ','size','sno')


# Register your models here.
admin.site.register(Product, ProductAdmin)
admin.site.register(Order)
admin.site.register(Equipment, EquipmentAdmin)
# admin.site.unregister(Group)




