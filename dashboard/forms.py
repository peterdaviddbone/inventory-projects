from django import forms
from .models import Product, Order, Equipment



class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['name','category','quantity']


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['product','order_quantity']



class EquipmentForm(forms.ModelForm):
    class Meta:
        model = Equipment
        fields = ['name','type','make','seno','status','mac','typ','size','sno']