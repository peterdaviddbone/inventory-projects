from django.db import models
from django.contrib.auth.models import User

# Create your models here.
CATEGORY = (
    ('Stationary', 'Stationary'),
    ('Electronics', 'Electronics'),
    ('Food', 'Food'),
)
class Product(models.Model):
    name = models.CharField(max_length=100, null=True)
    category = models.CharField(max_length=20, choices=CATEGORY, null=True)
    quantity = models.PositiveIntegerField(null=True)

    def __str__(self):
        return f'{self.name}-{self.quantity}'

    class Meta:
        verbose_name_plural = 'Product'


class Order(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True)
    staff = models.ForeignKey(User,models.CASCADE, null=True)
    order_quantity = models.PositiveIntegerField(null=True)
    date = models.DateTimeField(auto_now_add=True)



    class Meta:
        verbose_name_plural = 'Order'


    def __str__(self):
         return f'{self.product} ordered by {self.staff}'


class Equipment(models.Model):
    name = models.CharField(max_length=100, null=True)
    type = models.CharField(max_length=100, null=True)
    make = models.CharField(max_length=100, null=True)
    seno = models.CharField(max_length=100, null=True)
    status = models.CharField(max_length=100, null=True)
    mac = models.CharField(max_length=20, null=True)
    typ = models.CharField(max_length=20, null=True)
    size = models.CharField(max_length=20, null=True)
    sno = models.CharField(max_length=20, null=True)


    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name_plural = 'Equipment'













