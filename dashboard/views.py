from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from .models import Product, Order, Equipment
from .forms import ProductForm, OrderForm, EquipmentForm
from django.contrib.auth.models import User
from django.contrib import messages

# Create your views here.


@login_required
def index(request):
    orders = Order.objects.all()
    if request.method == 'POST':
        form = OrderForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.staff = request.user
            instance.save()
            return redirect('dashboard-index')
    else:
        form = OrderForm()
    context = {
        'orders': orders,
        'form': form,


    }

    return render(request,'dashboard/index.html', context)

@login_required
def product_update(request, pk):
    item = Product.objects.get(id=pk)
    if request.method == 'POST':
        form = ProductForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect('dashboard-product')

    else:
        form = ProductForm(instance=item)
    context = {
        'form': form,

    }
    return render(request, 'dashboard/product_update.html', context)
@login_required
def staff_detail(request,pk):
    workers = User.objects.get(id=pk)
    context = {
        'workers': workers,
    }


    return render(request, 'dashboard/staff_detail.html', context)






@login_required
def staff(request):
    workers = User.objects.all()
    context = {
        'workers': workers
    }

    return render(request, 'dashboard/staff.html',context)


@login_required
def product(request):
     items = Product.objects.all() #using 0RM
     #items = Product.objects.raw('SELECT *FROM dashboard_product')

     if request.method == 'POST':
         form = ProductForm(request.POST)
         if form.is_valid():
             form.save()
             product_name = form.cleaned_data.get('name')
             messages.success(request, f'{ product_name } has been added')

             return redirect('dashboard-product')


     else:
         form = ProductForm()

     context = {
        'items': items,
         'form': form,
     }
     return render(request, 'dashboard/product.html', context)


@login_required
def order(request):
    orders = Order.objects.all()

    context = {
        'orders': orders,

    }

    return render(request, 'dashboard/order.html', context)

@login_required
def product_delete(request, pk):
    item = Product.objects.get(id=pk)
    if request.method=='POST':
        item.delete()
        return redirect('dashboard-product')
    return render(request, 'dashboard/product_delete.html')



@login_required
def equipment(request):
     equipments = Equipment.objects.all() #using 0RM
     #equipments = Equipment.objects.raw('SELECT *FROM dashboard_product')

     if request.method == 'POST':
         form = EquipmentForm(request.POST)
         if form.is_valid():
             form.save()
             name = form.cleaned_data.get('name')
             messages.success(request, f'{ name } has been added')

             return redirect('dashboard-equipment')


     else:
         form = EquipmentForm()

     context = {
        'equipments': equipments,
         'form': form,
     }
     return render(request, 'dashboard/equipment.html', context)



@login_required
def equipment_update(request, pk):
    equipment = Equipment.objects.get(id=pk)
    if request.method == 'POST':
        form = EquipmentForm(request.POST, instance=equipment)
        if form.is_valid():
            form.save()
            return redirect('dashboard-equipment')

    else:
        form = EquipmentForm(instance=equipment)
    context = {
        'form': form,

    }
    return render(request, 'dashboard/equipment_update.html', context)



@login_required
def equipment_delete(request, pk):
    equipment = Equipment.objects.get(id=pk)
    if request.method=='POST':
        equipment.delete()
        return redirect('dashboard-equipment')
    return render(request, 'dashboard/equipment_delete.html')





